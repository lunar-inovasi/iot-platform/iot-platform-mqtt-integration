# IoT Platform MQTT Integration

## Getting started
IoT platform bisa menerima data melalui protokol MQTT yang dikirimkan oleh perangkat IoT ke MQTT Broker Mosquitto.

## MQTT Topics
### Topic data sensor
```
{device_id}/meter/data
```
Device mengirimkan hasil pembacaan sensor ke topic tersebut dengan format JSON. Format message data sensor sebagai berikut:
```json
{
  "devID": "SLR_75c6dda8", // Device id unik untuk setiap perangkat
  "datetime": "2023-10-27 17:45:00",
  "data": {
    "Active_Energy_Demand_Consumed": 1270598, // required. hasil pembacaan energy listrik increment.
    "Active_Power_Demand": 90, // required. Hasil pembacaan beban listrik.
    "Active_Energy_Demand_to_Grid": "124527",
    "Reactive_Power_Demand": -80,
    "currentA": 0.06,
    "currentB": 0,
    "currentC": 0,
    "pfA": 0,
    "pfA_decimal": 0,
    "pfA_quadran": 1,
    "pfB": 0,
    "pfB_decimal": 0,
    "pfB_quadran": 1,
    "pfC": 0,
    "pfC_decimal": 0,
    "pfC_quadran": 1,
    "power": 90,
    "thdA": 1.54,
    "thdB": 0,
    "thdC": 0,
    "var": -80,
    "voltageAN": 207,
    "voltageBN": 0,
    "voltageCN": 0
  }
}

```
Hasil pembacaan dikirimkan 5 menit sekali.

### Topic status perangkat
Device mengirimkan status perangkat pada topic sebagai berikut:
```
{device_id}/dev/status
```
Contoh pesan untuk status perangkat online:
```json
{
    "devID": "SLR_75c6dda8", // Device id unik untuk setiap perangkat
    "online": true, // boolean
}
```
Pesan perangkat online bersifat retained dan dikirimkan setiap kali perangkat berhasil melakukan koneksi ke MQTT Broker.

Untuk mengirimkan status perangkat offline. Gunakan pesan retained berikut untuk Last Will and Testament (LWT). 
```json
{
    "devID": "SLR_75c6dda8", // Device id unik untuk setiap perangkat
    "online": false, // boolean
}
```

